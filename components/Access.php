<?php

namespace app\components;

use app\modules\user\models\User;

class Access
{
    /**
     * Check access of current user for $action
     *
     * @todo необходимо реализовать систему прав
     * Сейчас права (на все) есть только у админа, параметр $action игнорируется
     *
     * @param string $action
     * @return bool
     */
    public static function check($action)
    {
        if (User::isGuest()) {
            return false;
        }

        /**
         * @var User $user
         */
        $user = User::findOne(Session::get('userId'));

        if ($user->login == 'admin') {
            return true;
        }

        return false;
    }
}
