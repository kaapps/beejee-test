<?php

namespace app\components;

use app\components\db\Db;
use app\modules\task\controllers\DefaultController;

class App
{
    /**
     * @var Route
     */
    private static $route;

    /**
     * @var Controller
     */
    private static $controller;

    /**
     * @var Db
     */
    private static $db;

    /**
     * Start App
     *
     * * Init session
     * * Init Db
     * * Init Route
     * * Find Controller by Route
     * * Run Controller's page
     */
    public static function init()
    {
        Session::init();
        self::setDb(new Db());
        self::setRoute(new Route());
        self::findControllerByRoute();
        self::runControllerPage();
    }

    /**
     * @return Route
     */
    public static function getRoute()
    {
        return self::$route;
    }

    /**
     * @return Controller
     */
    public static function getController()
    {
        return self::$controller;
    }

    /**
     * @return Db
     */
    public static function getDb()
    {
        return self::$db;
    }

    /**
     * @param Db $db
     */
    private static function setDb(Db $db)
    {
        self::$db = $db;
    }

    /**
     * @param Controller $controller
     */
    private static function setController(Controller $controller)
    {
        self::$controller = $controller;
    }

    /**
     * @param Route $route
     */
    private static function setRoute(Route $route)
    {
        self::$route = $route;
    }

    /**
     * Find controller by Route
     */
    private static function findControllerByRoute()
    {
        $module = self::getRoute()->getModule();
        $controller = self::getRoute()->getController();

        $className = "app\\modules\\$module\\controllers\\{$controller}Controller";

        if (class_exists($className)) {
            self::setController(new $className);
        } else {
            self::setController(new DefaultController());
        }
    }

    /**
     * Run controller page
     */
    private static function runControllerPage()
    {
        if (method_exists(self::getController(), self::getRoute()->getPage())) {
            call_user_func([self::getController(), self::getRoute()->getPage()]);
        } else {
            call_user_func([self::getController(), 'pageIndex']);
        }
    }
}
