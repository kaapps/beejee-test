<?php

namespace app\components;

class Config
{
    const DEFAULT_MODULE = 'page';
    const DB_HOST = 'localhost';
    const DB_NAME = 'beeJeeTask';
    const DB_USER = 'root';
    const DB_PASSWORD = '1187973';
    const PRIVATE_HASH = '3176a133b07012f0a8aa42524bc64ce5';
    const IMAGE_MAX_WIDTH = 320;
    const IMAGE_MAX_HEIGHT = 240;

    public static $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
}
