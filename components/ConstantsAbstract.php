<?php

namespace app\components;

abstract class ConstantsAbstract
{
    /**
     * @var array
     */
    protected static $texts = [];

    /**
     * Returns constants text list
     *
     * @return array
     */
    public static function getTextList()
    {
        return static::$texts;
    }

    /**
     * Get constant text
     *
     * @param int $constant
     * @return string
     */
    public static function getText($constant)
    {
        if (isset(static::$texts[$constant])) {
            return static::$texts[$constant];
        }

        return null;
    }
}
