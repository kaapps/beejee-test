<?php

namespace app\components;

/**
 * Class Controller
 *
 * @package app\components
 */
abstract class Controller
{
    /**
     * Default Page
     *
     * For example:
     * $this->render('index');
     */
    abstract public function pageIndex();

    /**
     * @param string $name
     * @param array $data
     */
    protected function render($name, array $data = [])
    {
        include Path::toLayout('header');

        $this->renderPart($name, $data);

        include Path::toLayout('footer');
    }

    /**
     * @param string $name
     * @param array $data
     */
    protected function renderPart($name, array $data = [])
    {
        extract($data);

        include Path::toView($this->moduleName(), $name);
    }

    /**
     * @return string
     */
    abstract protected function moduleName();
}
