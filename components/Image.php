<?php

namespace app\components;

use Imagick;

class Image
{
    /**
     * @var Imagick
     */
    private $image;

    /**
     * @var string
     */
    private $path;

    /**
     * Image constructor.
     * @param string $file
     */
    public function __construct($file)
    {
        $this->setPath($file);
        $this->setImage(new Imagick($this->getPath()));
    }

    /**
     * Resize and save image
     *
     * @param string $file
     */
    public function resize($width, $height)
    {
        $this->getImage()->adaptiveResizeImage($width, $height);
        file_put_contents($this->getPath(), $this->getImage());
    }

    /**
     * Resize image if it bigger than $width or $height
     *
     * @param int $width
     * @param int $height
     */
    public function resizeIfBiggerThan($width, $height)
    {
        if (!$this->isWidthBiggerThan($width) && !$this->isHeightBiggerThan($height)) {
            return;
        }

        if ($this->isWidthBiggerThan($width)) {
            $this->resizeByWidth($width);
        }

        if ($this->isHeightBiggerThan($height)) {
            $this->resizeByHeight($height);
        }
    }

    /**
     * Resize image by $width
     *
     * @param int $width
     */
    public function resizeByWidth($width)
    {
        $ratio = $width / $this->getImage()->getImageWidth();
        $height = $this->getImage()->getImageHeight() * $ratio;
        $this->resize($width, $height);
    }

    /**
     * Resize image by $height
     *
     * @param int $height
     */
    public function resizeByHeight($height)
    {
        $ratio = $height / $this->getImage()->getImageHeight();
        $width = $this->getImage()->getImageWidth() * $ratio;
        $this->resize($width, $height);
    }

    /**
     * @return Imagick
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param int $width
     * @return bool
     */
    private function isWidthBiggerThan($width)
    {
        return ($this->getImage()->getImageWidth() > $width);
    }

    /**
     * @param int $height
     * @return bool
     */
    private function isHeightBiggerThan($height)
    {
        return ($this->getImage()->getImageHeight() > $height);
    }

    /**
     * @param string $path
     */
    private function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @param Imagick $image
     */
    private function setImage($image)
    {
        $this->image = $image;
    }
}
