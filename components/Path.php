<?php

namespace app\components;

class Path
{
    /**
     * Path to root directory
     */
    const ROOT = __DIR__ . '/..';

    /**
     * @param string $moduleName
     * @param string $viewName
     * @return string
     */
    public static function toView($moduleName, $viewName)
    {
        return self::ROOT . '/modules/' . $moduleName . '/views/' . $viewName . '.php';
    }

    /**
     * @param string $layerName
     * @return string
     */
    public static function toLayout($layerName)
    {
        return self::ROOT . '/layouts/' . $layerName . '.php';
    }

    /**
     * Path to folder for uploaded files
     */
    public static function toUploads()
    {
        return self::ROOT . '/uploads';
    }
}
