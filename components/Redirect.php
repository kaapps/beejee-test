<?php

namespace app\components;

/**
 * Class Redirect
 *
 * @package app\components
 */
class Redirect
{
    /**
     * @param string $url
     */
    public static function to($url)
    {
        header('Location: ' . $url);
        exit();
    }

    /**
     * Redirect to home
     */
    public static function toHome()
    {
        self::to('/');
    }

    /**
     * Redirect to not found page
     */
    public static function toNotFoundPage()
    {
        self::to('/page/default/404');
    }
}
