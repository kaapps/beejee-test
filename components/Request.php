<?php

namespace app\components;

/**
 * Class Request
 *
 * @package app\components
 */
class Request
{
    /**
     * @return string
     */
    public static function getUri()
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * @return bool
     */
    public static function isPost()
    {
        return empty($_POST) ? false : true;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function post($key, $defaultValue = null)
    {
        return isset($_POST[$key]) ? $_POST[$key] : $defaultValue;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function get($key, $defaultValue = null)
    {
        return isset($_GET[$key]) ? $_GET[$key] : $defaultValue;
    }
}
