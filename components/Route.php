<?php

namespace app\components;

/**
 * Class Route
 *
 * @package app\components
 */
class Route
{
    /**
     * @var string
     */
    private $module = Config::DEFAULT_MODULE;

    /**
     * @var string
     */
    private $controller = "Default";

    /**
     * @var string
     */
    private $page = "pageIndex";

    /**
     * Route constructor.
     */
    public function __construct()
    {
        $uri = Request::getUri();

        if (empty($uri)) {
            return;
        }

        $uriParts = explode("/", $uri);

        $this->parseModelName($uriParts);
        $this->parseControllerName($uriParts);
        $this->parsePageName($uriParts);
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param string $module
     */
    private function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @param string $controller
     */
    private function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param string $page
     */
    private function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @param array $uriParts
     */
    private function parseModelName(array $uriParts)
    {
        if (!empty($uriParts[1])) {
            $this->setModule($uriParts[1]);
        }
    }

    /**
     * @param array $uriParts
     */
    private function parseControllerName(array $uriParts)
    {
        if (!empty($uriParts[2])) {
            $uriParts[2] = ucfirst($uriParts[2]);
            $this->setController($uriParts[2]);
        }
    }

    /**
     * @param array $uriParts
     */
    private function parsePageName(array $uriParts)
    {
        if (!empty($uriParts[3])) {
            $uriParts[3] = ucfirst($uriParts[3]);
            $this->setPage('page' . $uriParts[3]);
        }
    }
}
