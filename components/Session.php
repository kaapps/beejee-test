<?php

namespace app\components;

class Session
{
    /**
     * @param string $key
     * @param mixed $defValue
     * @return mixed
     */
    public static function get($key, $defValue = null)
    {
        return (isset($_SESSION[$key])) ? $_SESSION[$key] : $defValue;
    }

    /**
     * Start session
     */
    public static function init()
    {
        session_start();
    }

    /**
     * Delete all registered vars
     */
    public static function clear()
    {
        session_unset();
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }
}
