<?php

namespace app\components;

class Validate
{
    const FILTER_EMAIL = 1;
    const FILTER_NOT_EMPTY = 2;

    /**
     * @param string $var
     * @param array $filters List of filters for validate
     * @return bool
     */
    public static function filter($var, array $filters)
    {
        foreach ($filters as $filter) {
            $result = true;

            switch ($filter) {
                case self::FILTER_EMAIL:
                    $result = self::email($var);
                    break;

                case self::FILTER_NOT_EMPTY:
                    $result = self::notEmpty($var);
                    break;
            }

            if (!$result) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $var
     * @return mixed
     */
    public static function email($var)
    {
        return filter_var($var, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param string $var
     * @return bool
     */
    public static function notEmpty($var)
    {
        return empty($var) ? false : true;
    }
}
