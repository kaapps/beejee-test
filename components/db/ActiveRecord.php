<?php

namespace app\components\db;

use app\components\App;
use PDO;

/**
 * Class ActiveRecord
 *
 * @package app\components\db
 */
abstract class ActiveRecord extends Model
{
    /**
     * Returns all records
     *
     * @return array ActiveRecord[]
     */
    public static function findAll($orderBy = null)
    {
        $table = static::getTableName();

        $order = '';

        if ($orderBy !== null) {
            $order = 'ORDER BY ' . $orderBy . ' ASC';
        }

        $query = App::getDb()->getConnection()->query("
            SELECT * FROM $table
            $order 
        ");

        $results = null;

        while ($result = $query->fetchObject(static::class)) {
            $results[] = $result;
        }

        return $results;
    }

    /**
     * Returns one record by primary key
     *
     * @param int $primary
     * @return ActiveRecord
     */
    public static function findOne($primary)
    {
        $table = static::getTableName();
        $primary = (int)$primary;
        $primaryName = static::getPrimary();

        $query = App::getDb()->getConnection()->prepare("
            SELECT * FROM $table
            WHERE $primaryName = :primary
        ");

        $query->bindParam(':primary', $primary, PDO::PARAM_INT);
        $query->execute();

        $result = $query->fetchObject(static::class);

        return ($result) ? $result : null;
    }

    /**
     * @return string
     */
    public static function lastInsertedId()
    {
        return App::getDb()->getConnection()->lastInsertId();
    }

    /**
     * @return bool
     */
    public function save()
    {
        $fields = get_object_vars($this);
        $primary = $fields[static::getPrimary()];
        $fields = $this->dropNullsFromFields($fields);

        if ($primary === null) {
            return $this->insert($fields);
        }

        return $this->update($fields, $primary);
    }

    /**
     * @param array $fields
     * @return array
     */
    private function dropNullsFromFields(array $fields)
    {
        foreach ($fields as $key => $val) {
            if ($val === null) {
                unset($fields[$key]);
            }
        }

        return $fields;
    }

    /**
     * @param array $fields
     * @return bool
     */
    private function insert(array $fields)
    {
        $table = static::getTableName();

        $query = App::getDb()->getConnection()->prepare("
            INSERT INTO $table (" . implode(', ', array_keys($fields)) . ") 
            VALUES (:" . implode(', :', array_keys($fields)) . ")
        ");

        $result = $query->execute($fields);

        if ($result) {
            $this->{static::getPrimary()} = static::lastInsertedId();
        }

        return $result;
    }

    /**
     * @param array $fields
     * @param int $primary
     * @return bool
     */
    private function update(array $fields, $primary)
    {
        $table = static::getTableName();
        $primaryKey = static::getPrimary();
        $primary = (int)$primary;

        $set = '';

        foreach ($fields as $key => $val) {
            $set .= "$key = :$key,";
        }

        $set = substr($set, 0, -1);

        $query = App::getDb()->getConnection()->prepare("            
            UPDATE $table
            SET $set
            WHERE $primaryKey = $primary
            LIMIT 1
        ");

        return $query->execute($fields);
    }
}
