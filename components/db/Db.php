<?php

namespace app\components\db;

use app\components\Config;
use PDO;

/**
 * Class Db
 *
 * @package app\components\db
 */
class Db
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * Db constructor.
     * @param string $host
     * @param string $name
     */
    public function __construct()
    {
        $this->setConnection(new PDO(
            "mysql:host=" . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';',
            Config::DB_USER,
            Config::DB_PASSWORD
        ));
    }

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @param PDO $connection
     */
    private function setConnection($connection)
    {
        $this->connection = $connection;
    }
}
