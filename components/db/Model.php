<?php

namespace app\components\db;

use app\components\db\interfaces\ModelInterface;

abstract class Model implements ModelInterface
{
    /**
     * @return string Primary key name
     */
    public static function getPrimary()
    {
        return 'id';
    }
}
