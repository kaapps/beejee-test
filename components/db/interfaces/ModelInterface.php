<?php

namespace app\components\db\interfaces;

interface ModelInterface
{
    /**
     * @return string
     */
    public static function getTableName();
}
