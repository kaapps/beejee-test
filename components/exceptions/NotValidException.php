<?php

namespace app\components\exceptions;

class NotValidException extends \Exception
{
    /**
     * @var array
     */
    public $errors;

    /**
     * NotValidException constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }
}
