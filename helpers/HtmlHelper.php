<?php

namespace app\helpers;

class HtmlHelper
{
    /**
     * Generate options for selectBox
     *
     * @param array $list
     * @param string $firstElement
     * @return string
     */
    public static function generateOptions(array $list, $firstElement = null)
    {
        $result = '';

        if ($firstElement !== null) {
            $result .= '<option>' . $firstElement . '</option>';
        }

        foreach ($list as $key => $val) {
            $result .= "<option value='{$key}'>{$val}</option>";
        }

        return $result;
    }

    /**
     * Convert html special chars to text
     *
     * @param string $html
     * @return string
     */
    public static function toText($html)
    {
        return nl2br(htmlspecialchars($html));
    }
}
