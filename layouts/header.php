<?php

use app\modules\user\models\User;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossorigin="anonymous">

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- Our script -->
    <script src="/assets/js/main.js"></script>

</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">BeeJee-Task</a>
            </div>

            <ul class="nav navbar-nav">
                <li><a href="/task/default/create/">Добавить задачу</a></li>
                <li><a href="/task/default/list/">Список задач</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if (User::isGuest()) : ?>
                    <li><a href="/user/"><span class="glyphicon glyphicon-log-in"></span> Вход</a></li>
                <?php else : ?>
                    <li><a href="/user/default/logout/"><span class="glyphicon glyphicon-log-out"></span> Выход</a></li>
                <?php endif; ?>

            </ul>
    </nav>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">