<?php

namespace app\modules\file\controllers;

use app\components\Controller;
use app\components\Redirect;
use app\components\Request;
use app\modules\file\helpers\FileHelper;

class DefaultController extends Controller
{
    /**
     * Show image
     */
    public function pageIndex()
    {
        if (Request::get('file') === null || !FileHelper::fileNameValidate(Request::get('file'))) {
            Redirect::toNotFoundPage();
        }

        $this->showFile(Request::get('file'));
    }

    /**
     * @return string
     */
    protected function moduleName()
    {
        return 'file';
    }

    /**
     * @param string $fileName
     */
    private function showFile($fileName)
    {
        $file = FileHelper::pathToFile($fileName);

        if (!is_file($file)) {
            Redirect::toNotFoundPage();
        }

        header("Content-type: " . mime_content_type($file));
        readfile($file);
    }
}
