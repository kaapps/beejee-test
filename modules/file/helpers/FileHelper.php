<?php

namespace app\modules\file\helpers;

use app\components\Path;

class FileHelper
{
    /**
     * @param string $file
     * @return string
     */
    public static function pathToFile($file)
    {
        return Path::toUploads() . '/' . $file;
    }

    /**
     * @param string $name
     * @return int
     */
    public static function fileNameValidate($name)
    {
        return preg_match('/[a-z0-9_\.]/', $name);
    }

    /**
     * Returns lowercase file extension
     *
     * @param string $fileName
     * @return string
     */
    public static function getExtension($fileName)
    {
        return strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
    }
}
