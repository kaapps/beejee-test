<?php

namespace app\modules\file\models;

use app\modules\file\helpers\FileHelper;

class FileUpload
{
    /**
     * @var bool
     */
    private $success = false;

    /**
     * @var string
     */
    private $fileName;

    /**
     * FileUpload constructor.
     * @param string $inputName
     */
    public function __construct($inputName = 'file')
    {
        $fileName = $this->generateFileName($inputName);

        if (move_uploaded_file($_FILES[$inputName]['tmp_name'], FileHelper::pathToFile($fileName))) {
            $this->setFileName($fileName);
            $this->setSuccess(true);
        }
    }

    /**
     * Validate extension
     *
     * @param array $allow Allowed extension
     * @param string $inputName
     * @return bool
     */
    public static function extensionValidator(array $allow, $inputName = 'file')
    {
        return in_array(FileHelper::getExtension($_FILES[$inputName]['name']), $allow);
    }

    /**
     * Returns true if have file to upload
     *
     * @param string $inputName
     * @return bool
     */
    public static function hasFile($inputName = 'file')
    {
        return ($_FILES[$inputName]['error'] != UPLOAD_ERR_NO_FILE);
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    private function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @param boolean $success
     */
    private function setSuccess($success)
    {
        $this->success = $success;
    }

    private function generateFileName($inputName)
    {
        $fileName = md5(rand(1, 9999999) . time());
        $fileName .= '_' . md5(rand(1, 9999999));
        $fileName .= '.' . FileHelper::getExtension($_FILES[$inputName]['name']);

        return $fileName;
    }
}
