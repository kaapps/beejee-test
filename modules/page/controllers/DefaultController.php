<?php

namespace app\modules\page\controllers;

use app\components\Controller;

class DefaultController extends Controller
{
    /**
     * Default Page
     */
    public function pageIndex()
    {
        $this->render('index');
    }

    /**
     * Not found page
     */
    public function page404()
    {
        $this->render('404');
    }

    /**
     * @return string
     */
    protected function moduleName()
    {
        return 'page';
    }
}
