<?php

namespace app\modules\task\constants;

use app\components\ConstantsAbstract;

class TaskAccess extends ConstantsAbstract
{
    const EDIT = __CLASS__ . '/edit';
}
