<?php

namespace app\modules\task\constants;

use app\components\ConstantsAbstract;

class TaskStatus extends ConstantsAbstract
{
    const OPEN = 0;
    const IN_PROCESS = 1;
    const CLOSED = 2;
    const CANCELED = 3;

    protected static $texts = [
        self::OPEN => 'Открыт',
        self::IN_PROCESS => 'В процессе',
        self::CLOSED => 'Закрыт',
        self::CANCELED => 'Отменен',
    ];
}
