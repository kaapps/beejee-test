<?php

namespace app\modules\task\controllers;

use app\components\Access;
use app\components\Controller;
use app\components\Redirect;
use app\components\Request;
use app\modules\task\constants\TaskAccess;
use app\modules\task\constants\TaskStatus;
use app\modules\task\exceptions\TaskNotValidException;
use app\modules\task\models\Task;

/**
 * Class DefaultController
 *
 * @package app\modules\task\controllers
 */
class DefaultController extends Controller
{
    /**
     * Main page of the project
     */
    public function pageIndex()
    {
        Redirect::to('/task/default/list/');
    }

    /**
     * Create a task page
     */
    public function pageCreate()
    {
        $task = new Task();
        $this->createAndUpdate($task);
    }

    /**
     * Update a task page
     */
    public function pageUpdate()
    {
        if (!Access::check(TaskAccess::EDIT)) {
            Redirect::toNotFoundPage();
        }

        $task = Task::findOne(Request::get('id'));

        if ($task === null) {
            Redirect::toNotFoundPage();
        }

        $this->createAndUpdate($task);
    }

    /**
     * View task page
     */
    public function pageView()
    {
        $task = Task::findOne(Request::get('id'));

        if ($task === null) {
            Redirect::toNotFoundPage();
        }

        $this->render('view', [
            'model' => $task,
            'preview' => false,
        ]);
    }

    public function pagePreview()
    {
        $task = new Task();

        $this->load($task);

        $this->renderPart('view', [
            'model' => $task,
            'preview' => true,
        ]);
    }

    /**
     * View all tasks page
     */
    public function pageList()
    {
        if (in_array(Request::get('order'), ['id', 'userName', 'email', 'status'])) {
            $order = Request::get('order');
        } else {
            $order = null;
        }

        $tasks = Task::findAll($order);

        $this->render('list', [
            'modelList' => $tasks,
        ]);
    }

    /**
     * @return string
     */
    protected function moduleName()
    {
        return 'task';
    }

    /**
     * @param Task $task
     */
    private function createAndUpdate(Task $task)
    {
        $errors = null;

        if (Request::isPost()) {
            $this->load($task);

            try {
                if ($task->validate() && $task->save()) {
                    Redirect::to('/task/default/view/?id=' . $task->id);
                }
            } catch (TaskNotValidException $e) {
                $errors = $e->errors;
            }
        }

        $viewFile = ($task->id === null) ? 'create' : 'update';

        $this->render($viewFile, [
            'model' => $task,
            'errors' => $errors,
        ]);
    }

    /**
     * Fill model properties by post
     */
    private function load(Task $task)
    {
        $task->userName = Request::post('userName');
        $task->email = Request::post('email');
        $task->text = Request::post('text');

        if (Request::post('id') !== null) {
            $task->id = Request::post('id');
            $task->status = Request::post('status');
        } else {
            $task->status = TaskStatus::OPEN;
        }
    }
}
