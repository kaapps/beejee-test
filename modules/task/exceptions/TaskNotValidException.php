<?php

namespace app\modules\task\exceptions;

use app\components\exceptions\NotValidException;

class TaskNotValidException extends NotValidException
{
}
