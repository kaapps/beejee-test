<?php

namespace app\modules\task\models;

use app\components\Config;
use app\components\Image;
use app\modules\file\helpers\FileHelper;
use app\modules\file\models\FileUpload;
use app\modules\task\exceptions\TaskNotValidException;

class Task extends TaskBase
{
    /**
     * @return bool
     * @throws TaskNotValidException
     */
    public function save()
    {
        if (FileUpload::hasFile()) {
            $file = new FileUpload();

            if (!$file->isSuccess()) {
                throw new TaskNotValidException(['Ошибка загрузки файла']);
            }

            $this->fileName = $file->getFileName();

            $image = new Image(FileHelper::pathToFile($this->fileName));
            $image->resizeIfBiggerThan(Config::IMAGE_MAX_WIDTH, Config::IMAGE_MAX_HEIGHT);
        }

        return parent::save();
    }
}
