<?php

namespace app\modules\task\models;

use app\components\Config;
use app\components\db\ActiveRecord;
use app\components\Validate;
use app\modules\file\models\FileUpload;
use app\modules\task\exceptions\TaskNotValidException;

/**
 * Class TaskBase
 *
 * @package app\modules\task\models
 */
class TaskBase extends ActiveRecord
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $userName;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $text;

    /**
     * @var int A constant of {@see TaskStatus}
     * @see TaskStatus
     */
    public $status;

    /**
     * @var string
     */
    public $fileName;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'task';
    }

    /**
     * @return bool
     * @throws TaskNotValidException
     */
    public function validate()
    {
        $errors = [];

        if (!Validate::email($this->email)) {
            $errors[] = 'Введите валидную почту';
        }

        if (!Validate::notEmpty($this->text)) {
            $errors[] = 'Необходимо заполнить поле "Текст"';
        }

        if (FileUpload::hasFile() && !FileUpload::extensionValidator(Config::$allowedExtensions)) {
            $errors[] = 'Разрешено добавлять только файлы в форматах '
                . implode(', ', Config::$allowedExtensions);
        }

        if (!empty($errors)) {
            throw new TaskNotValidException($errors);
        }

        return true;
    }
}
