<?php

use app\modules\task\controllers\DefaultController;
use app\modules\task\models\Task;

/**
 * @var DefaultController $this
 * @var Task $model
 * @var array $errors
 */
?>

<div class="page-header">
    <h1>Добавить задачу</h1>
</div>

<form id="createTask" class="form-horizontal" method="post"  enctype="multipart/form-data">

    <?php $this->renderPart('parts/forms', ['model' => $model, 'errors' => $errors]); ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Добавить</button>
            <button type="button" class="btn btn-default" onclick="taskPreview()">Предпросмотр
            </button>
        </div>
    </div>
    <div id="taskPreview"></div>
</form>