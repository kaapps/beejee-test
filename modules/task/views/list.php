<?php
use app\modules\task\constants\TaskStatus;
use app\modules\task\models\Task;

/**
 * @var Task[] $modelList
 */
?>

<div class="page-header">
    <h1>Список задач</h1>
</div>

<table class="table">
    <thead>
    <tr>
        <th><a href="/task/default/list/?order=id">id</a></th>
        <th><a href="/task/default/list/?order=userName">Имя пользователя</a></th>
        <th><a href="/task/default/list/?order=email">E-m@il</a></th>
        <th><a href="/task/default/list/?order=status">Статус</a></th>
        <th>Просмотр</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($modelList as $model) :
        ?>
        <tr>
            <td><a href="/task/default/view/?id=<?= $model->id ?>"><?= $model->id ?></a></td>
            <td><?= $model->userName ?></td>
            <td><?= $model->email ?></td>
            <td><?= TaskStatus::getText($model->status) ?></td>
            <td><a href="/task/default/view/?id=<?= $model->id ?>">Перейти</a></td>
        </tr>
        <?php
    endforeach;
    ?>

    </tbody>
</table>
