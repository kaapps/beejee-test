<?php

use app\modules\task\models\Task;

/**
 * @var Task $model
 * @var array $errors
 */

if ($errors !== null) {
    print '<div class="alert alert-danger" role="alert">' . implode('<br>', $errors) . '</div>';
}

if ($model->id !== null) :
    ?>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">id</label>
        <div class="col-sm-10">
            <p class="form-control"><?= $model->id ?></p>
            <input type="hidden" name="id" value="<?= $model->id ?>"  />
        </div>
    </div>

<?php endif; ?>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Имя</label>
    <div class="col-sm-10">
        <input type="text" name="userName" value="<?= $model->userName ?>" class="form-control" placeholder="Иван"/>
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">E-m@il <span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="email" name="email" value="<?= $model->email ?>" class="form-control" placeholder="ivan@mail.ru"/>
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Текст <span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <textarea class="form-control" name="text" rows="4"
                  placeholder="Подробно опишите задачу"><?= $model->text ?></textarea>
    </div>
</div>

<div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Прикрепите файл</label>
    <div class="col-sm-10">
        <input type="file" name="file" class="form-control" />
    </div>
</div>
