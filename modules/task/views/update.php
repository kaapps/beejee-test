<?php

use app\helpers\HtmlHelper;
use app\modules\task\constants\TaskStatus;
use app\modules\task\controllers\DefaultController;
use app\modules\task\models\Task;

/**
 * @var DefaultController $this
 * @var Task $model
 * @var array $errors
 */
?>

<div class="page-header">
    <h1>Редактировать задачу</h1>
</div>

<form class="form-horizontal" method="post" enctype="multipart/form-data" >

    <?php $this->renderPart('parts/forms', ['model' => $model, 'errors' => $errors]); ?>

    <div class="form-group">
        <label for="inputStatus" class="col-sm-2 control-label">Статус</label>
        <div class="col-sm-10">
            <select name="status" id="inputStatus" class="form-control">
                <?= HtmlHelper::generateOptions(TaskStatus::getTextList()) ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Изменить</button>
        </div>
    </div>

</form>