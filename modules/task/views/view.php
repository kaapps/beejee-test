<?php

use app\components\Access;
use app\helpers\HtmlHelper;
use app\modules\task\constants\TaskAccess;
use app\modules\task\constants\TaskStatus;
use app\modules\task\controllers\DefaultController;
use app\modules\task\models\Task;

/**
 * @var DefaultController $this
 * @var Task $model
 * @var bool $preview
 */

if (!$preview) : ?>
    <div class="page-header">
        <h1>Задача #<?= $model->id ?></h1>
    </div>
<?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Задача</div>
                <div class="panel-body"><?= HtmlHelper::toText($model->text) ?></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Имя</div>
                <div class="panel-body"><?= $model->userName ?></div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">E-m@il</div>
                <div class="panel-body"><?= $model->email ?></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Статус</div>
                <div class="panel-body"><?= TaskStatus::getText($model->status) ?></div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Файл</div>
                <div class="panel-body">
                    <?php if ($model->fileName !== null) : ?>
                        <a href="/file/default/?file=<?= $model->fileName ?>" target="_blank">Просмотр</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

<?php if (!$preview && Access::check(TaskAccess::EDIT)) : ?>

    <div class="row">
        <div class="col-md-12">
            <a href="/task/default/update/?id=<?= $model->id ?>" class="btn btn-info"> Редактировать</a>
        </div>
    </div>

<?php endif; ?>