<?php

namespace app\modules\user\controllers;

use app\components\Controller;
use app\components\Redirect;
use app\components\Request;
use app\modules\user\models\User;

class DefaultController extends Controller
{
    /**
     * Alias of authorization page
     */
    public function pageIndex()
    {
        $this->pageLogin();
    }

    /**
     * Authorization page
     */
    public function pageLogin()
    {
        $error = false;
        $successLogout = Request::get('logout', false);

        if (Request::isPost()) {
            if (User::authorize(Request::post('login'), Request::post('password'))) {
                Redirect::toHome();
            } else {
                $error = true;
            }
        }

        $this->render('index', [
            'error' => $error,
            'successLogout' => $successLogout,
        ]);
    }

    /**
     * Logout page
     */
    public function pageLogout()
    {
        User::logout();
        Redirect::to('/user/default/login/?logout=1');
    }

    /**
     * @return string
     */
    protected function moduleName()
    {
        return 'user';
    }
}
