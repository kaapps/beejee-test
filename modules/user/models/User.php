<?php

namespace app\modules\user\models;

use app\components\App;
use app\components\Config;
use app\components\Session;
use PDO;

class User extends UserBase
{
    /**
     * @return bool
     */
    public static function isGuest()
    {
        return (Session::get('userId') === null) ? true : false;
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
    public static function authorize($login, $password)
    {
        $table = self::getTableName();
        $passwordHash = md5(Config::PRIVATE_HASH . $password);

        //todo реализовать в ActiveRecord
        $query = App::getDb()->getConnection()->prepare("
            SELECT * FROM $table
            WHERE login = :login
            AND password = :password
            LIMIT 1
        ");

        $query->bindParam(':login', $login, PDO::PARAM_STR);
        $query->bindParam(':password', $passwordHash, PDO::PARAM_STR);
        $query->execute();

        $user = $query->fetchObject(static::class);

        if (!$user) {
            return false;
        }

        Session::set('userId', $user->id);
        return true;
    }

    /**
     * Logout
     */
    public static function logout()
    {
        Session::clear();
    }
}
