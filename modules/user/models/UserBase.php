<?php

namespace app\modules\user\models;

use app\components\db\ActiveRecord;

/**
 * Class UserBase
 *
 * @package app\modules\user\models
 */
class UserBase extends ActiveRecord
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $name;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'user';
    }
}
