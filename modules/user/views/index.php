<div class="page-header">
    <h1>Вход</h1>
</div>

<?php
/**
 * @var bool $error
 * @var bool $successLogout
 */

if ($error) {
    print '<div class="alert alert-danger" role="alert">Не правильный логин и/или пароль</div>';
}

if ($successLogout) {
    print '<div class="alert alert-success" role="alert">Вы успешно вышли из системы</div>';
}
?>

<form class="form-horizontal" action="/user/default/login/" method="post">
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Логин</label>
        <div class="col-sm-10">
            <input type="text" name="login" class="form-control" id="inputEmail3" placeholder="Логин">
        </div>
    </div>

    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Пароль</label>
        <div class="col-sm-10">
            <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Пароль">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Вход</button>
        </div>
    </div>
</form>