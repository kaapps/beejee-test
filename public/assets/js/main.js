/**
 * Task preview
 */
function taskPreview() {
    $.ajax({
        type: 'POST',
        url: '/task/default/preview/',
        data: $('#createTask').serializeArray(),
        success: function(data) {
            $('#taskPreview').html(data);
        }
    });
}
