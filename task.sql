CREATE TABLE task
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    userName VARCHAR(255),
    email VARCHAR(255),
    text TEXT,
    status TINYINT(4) NOT NULL,
    fileName VARCHAR(255)
);
