CREATE TABLE user
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    login VARCHAR(255),
    password VARCHAR(32),
    name VARCHAR(255)
);

INSERT INTO user (login, password, name)
VALUES ('admin', '52a8e8bad7093a5d26aae9abfb282bc4', 'BigBoss');